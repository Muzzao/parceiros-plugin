<?php
/*
Plugin Name: Image Linker
Plugin URI:  www.gestaoativa.com.br
Description: Add company logos and links to your widget sidebar
Version:     0.1
Author:      Mahmod A. Issa - Equipe Gestão Ativa
Author URI:  http://www.gestaoativa.com.br/Muh
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/


    include (__DIR__ . '/install.php');
    // include (__DIR__ . '/uninstall.php');


    class wp_my_plugin extends WP_Widget {

		// constructor
		function wp_my_plugin() {      
			parent::WP_Widget(false, $name = __('Parceiros', 'wp_widget_plugin') );
		}

		// widget form creation
		function form($instance) {	
			if($instance){
				$title = esc_attr($instance['title']);
				$item = esc_attr($instance['item']);
				$slideMove = esc_attr($instance['slideMove']);
				$speed = esc_attr($instance['speed']);
				$auto = esc_attr($instance['auto']);
			}else{
				$title='';
				$item=1;
				$slideMove=1;
				$speed=400;
				$auto=true;
			} ?>

			<p>
				<label for="<?php echo $this->get_field_id('title') ;?>"><?php _e('Título do Widget', 'wp_widget_plugin'); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
			</p>

			<p>
				<label for="<?php echo $this->get_field_id('item') ;?>"><?php _e('Quantidade de itens a ser exibido', 'wp_widget_plugin'); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id('item'); ?>" name="<?php echo $this->get_field_name('item'); ?>" type="text" value="<?php echo $item; ?>" />
			</p>

			<p>
				<label for="<?php echo $this->get_field_id('slideMove') ;?>"><?php _e('Número de slides por movimento', 'wp_widget_plugin'); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id('slideMove'); ?>" name="<?php echo $this->get_field_name('slideMove'); ?>" type="text" value="<?php echo $slideMove; ?>" />
			</p>

			<p>
				<label for="<?php echo $this->get_field_id('speed') ;?>"><?php _e('Velocidade', 'wp_widget_plugin'); ?> em Milisegundos</label>
				<input class="widefat" id="<?php echo $this->get_field_id('speed'); ?>" name="<?php echo $this->get_field_name('speed'); ?>" type="text" value="<?php echo $speed; ?>" />
			</p>

			<p>
				<label for="<?php echo $this->get_field_id('auto') ;?>"><?php _e('Automático', 'wp_widget_plugin'); ?></label>
				<select name="<?php echo $this->get_field_name('auto'); ?>"  class="widefat" id="<?php echo $this->get_field_id('auto'); ?>">
					<option value="<?php echo $auto; ?>">Sim</option>
					<option value="false">Não</option>
				</select>
			</p>


		<?php	}

		// widget update
		function update($new_instance, $old_instance) {
			$instance = $old_instance;
			// Fields
				$instance['title'] = strip_tags($new_instance['title']);
				$instance['item'] = strip_tags($new_instance['item']);
				$instance['slideMove'] = strip_tags($new_instance['slideMove']);
				$instance['speed'] = strip_tags($new_instance['speed']);
				$instance['auto'] = strip_tags($new_instance['auto']);
				return $instance;
		}
		// widget display
		function widget($args, $instance) {
			extract( $args );
		   // these are the widget options
		   $title = apply_filters('widget_title', $instance['title']);
		   echo $before_widget;
			   // Display the widget
			   if ( $title ) {
			      echo "<h5>" . $title . '</h5>';
			   }
			   echo '<div class="widget-text wp_widget_plugin_box"> <ul id="parceiro-slider" class="content-slider">';

				   // Check if title is set
				   // The Query
				   $args  = array(
				   		"post_type"=>"parceiro",
				   		"posts_per_page"=> -1,
				   		"nopaging" => true,
				   		"orderby" => "menu_order"
				   	);
					$query = new WP_Query($args);

					// The Loop
					if ($query->have_posts())   {
						while ( $query->have_posts() ) : $query->the_post(); 
						$link = link_get_meta( 'link_link_do_parceiro' );
						if ( $link ) { ?>
							<li><a target="_blank" href="<?php echo $link; ?>"> <?php the_post_thumbnail(); ?></a></li>
						<?php } else{
							echo "<li>".get_the_post_thumbnail()."</li>";
						} ?>
						<?php endwhile; 
					} else {
						echo "Nenhum post exibido";
					}

					// Restore original Post Data
					wp_reset_postdata();
			   
			   echo '</ul></div>';
		   echo $after_widget;
		}
	}

	// register widget
	add_action('widgets_init', create_function('', 'return register_widget("wp_my_plugin");'));


	function pluginprefix_deactivation() {
		
	    // Clear the permalinks to remove our post type's rules
	    	flush_rewrite_rules();
	}
	register_deactivation_hook( __FILE__, 'pluginprefix_deactivation' );
	
